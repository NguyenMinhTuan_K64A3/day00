-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 07, 2022 at 06:01 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */
;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */
;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */
;
/*!40101 SET NAMES utf8mb4 */
;

CREATE DATABASE qlsv;
--
-- Database: `qlsv`
--
-- --------------------------------------------------------
--
-- Table structure for table `dmkhoa`
--
CREATE TABLE `qlsv`.`dmkhoa` (
  `MAKH` VARCHAR(6) NOT NULL,
  `TenKhoa` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`MAKH`)
) ENGINE = InnoDB;
-- --------------------------------------------------------
--
-- Table structure for table `sinhvien`
--
CREATE TABLE `qlsv`.`sinhvien` (
  `MaSV` VARCHAR(6) NOT NULL,
  `HoSV` VARCHAR(30) NOT NULL,
  `TenSV` VARCHAR(15) NOT NULL,
  `GioiTinh` CHAR(1) NOT NULL,
  `NgaySinh` DATETIME NOT NULL,
  `NoiSinh` VARCHAR(50) NOT NULL,
  `DiaChi` VARCHAR(50) NOT NULL,
  `MaKH` VARCHAR(6) NOT NULL,
  `HocBong` INT NOT NULL,
  PRIMARY KEY (`MaSV`)
) ENGINE = InnoDB;

ALTER TABLE `sinhvien`
ADD CONSTRAINT `FK_DMKhoaSinhVien` FOREIGN KEY (`MaKH`) REFERENCES `dmkhoa`(`MAKH`) ON DELETE RESTRICT ON UPDATE RESTRICT;

COMMIT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */
;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */
;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */
;